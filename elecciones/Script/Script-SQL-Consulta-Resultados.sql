-- listar candidatos de Trelew - INTENDENTE
select lp.nom_lista, sum(telv.votos) from lista_partido_localidad as lpl inner join lista_partido as lp on lp.id = lpl.lista_partido_id inner join telegrama_acta_votos as telv on telv.lista_partido_loc_id = lpl.id 
where lpl.categoria_id =4 and localidad_id=2
group by lp.nom_lista
order by 2 desc;



-- listar candidatos de Trelew - CONCEJALES
select lp.nom_lista, sum(telv.votos) from lista_partido_localidad as lpl inner join lista_partido as lp on lp.id = lpl.lista_partido_id inner join telegrama_acta_votos as telv on telv.lista_partido_loc_id = lpl.id 
where lpl.categoria_id =5 and localidad_id=2
group by lp.nom_lista
order by 2 desc;
