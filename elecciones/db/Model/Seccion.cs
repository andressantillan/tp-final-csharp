﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class Seccion 
    {
        //"id","nombre"
        #region variables locales
        private int _id;
		private string _nombre;
        
        #endregion
        
        #region propiedades publicas
				
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        #endregion

        public override string ToString()
        {
            return this._nombre;
        }
    }
}
