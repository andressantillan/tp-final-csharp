﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class ListaPartidoLocalidad
    {
        //"id","lista_partido_id", "categoria_id", "localidad_id"
        #region variables locales
        private int _id;
		private int _lista_partido_id;
        private int _categoria_id;
        private int _localidad_id;
        #endregion

        private ListaPartido _listapartido=null;

        public ListaPartido ListaPartido
        {
            get {
                if (this._listapartido == null)
                    _listapartido = ManagerDB<ListaPartido>.findbyKey(this._lista_partido_id);
                return _listapartido; }
            set { _listapartido = value; }
        }
        private CategoriaEleccion _categoriaEleccion=null;

        public CategoriaEleccion CategoriaEleccion
        {
            get {
                if (this._categoriaEleccion == null)
                    _categoriaEleccion = ManagerDB<CategoriaEleccion>.findbyKey(this._categoria_id);
                return _categoriaEleccion; }
            set { _categoriaEleccion = value; }
        }
        private Localidad _localidad=null;

        public Localidad Localidad
        {
            get {
                if (_localidad == null)
                    _localidad = ManagerDB<Localidad>.findbyKey(this._localidad_id);
                return _localidad; }
            set { _localidad = value; }
        }


        #region propiedades publicas
				
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Lista_Partido_Id
        {
            get { return _lista_partido_id; }
            set { _lista_partido_id = value; }
        }

        public int Categoria_Id
        {
            get { return _categoria_id; }
            set { _categoria_id = value; }
        }

        public int Localidad_Id
        {
            get { return _localidad_id; }
            set { _localidad_id = value; }
        }
        #endregion
    }
}
