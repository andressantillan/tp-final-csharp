﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class TelegramaActaVotos
    {
        //"id","teleg_id", "lista_partido_loc_id", "votos"
        
        #region variables locales
        private int _id;
        private int _teleg_id;
		private int _lista_partido_loc_id;
        private int _votos;
        #endregion

        private TelegramaActa _telegrama=null;

        public TelegramaActa Telegrama
        {
            get {
                if (_telegrama == null)
                    _telegrama = ManagerDB<TelegramaActa>.findbyKey(this._teleg_id);
                return _telegrama; }
            set { _telegrama = value; }
        }

        private ListaPartidoLocalidad _listaPartidoLocalidad=null;

        public ListaPartidoLocalidad ListaPartidoLocalidad
        {
            get {
                if (_listaPartidoLocalidad == null)
                    _listaPartidoLocalidad = ManagerDB<ListaPartidoLocalidad>.findbyKey(this._lista_partido_loc_id);
                return _listaPartidoLocalidad; }
            set { _listaPartidoLocalidad = value; }
        }


        
        #region propiedades publicas
				
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Teleg_Id
        {
            get { return _teleg_id; }
            set { _teleg_id = value; }
        }

        public int Lista_Partido_Loc_Id
        {
            get { return _lista_partido_loc_id; }
            set { _lista_partido_loc_id = value; }
        }

        public int Votos
        {
            get { return _votos; }
            set { _votos = value; }
        }

        #endregion
    }
}
