﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class TelegramaActa 
    {
        //"id","nro_mesa", "fecha", "cargado"
        
        #region variables locales
        private int _id;
        private int _nro_mesa;
		private DateTime _fecha;
        private bool _cargado;
        
        public TelegramaActa()
        {
            this._fecha = DateTime.Today;
        }

        #endregion

        private Mesa _mesa=null;

        public Mesa Mesa
        {
            get {
                if (this._mesa == null)
                    _mesa = ManagerDB<Mesa>.findbyKey(this.Nro_Mesa);
                return _mesa; }
            set { _mesa = value; }
        }

        
        #region propiedades publicas
				
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Nro_Mesa
        {
            get { return _nro_mesa; }
            set { _nro_mesa = value; }
        }

        public DateTime Fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        public bool Cargado
        {
            get { return _cargado; }
            set { _cargado = value; }
        }

        #endregion
    }
}
