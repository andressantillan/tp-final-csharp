﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class Circuito 
    {
        //"id","letra", "secc_id", "nombre", "localidad_id"  
        #region variables locales
        private int _id;
		private string _letra;
        private int _secc_id;
        private string _nombre;
        private int _localidad_id;
        #endregion

        private Seccion _seccion=null;

        public Seccion Seccion
        {
            get {
                if (_seccion == null)
                {
                    _seccion = ManagerDB<Seccion>.findbyKey(this._secc_id);
                }
                return _seccion; }
            set { _seccion = value; }
        }

        private Localidad _localidad;

        public Localidad Localidad
        {
            get {
                if (_localidad == null)
                {
                    _localidad = ManagerDB<Localidad>.findbyKey(this._localidad_id);
                }
                return _localidad; }
            set { _localidad = value; }
        }
        
        #region propiedades publicas
				
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        
		public string Letra
        {
            get { return _letra; }
            set { _letra = value; }
        }


        public int Secc_Id
        {
            get { return _secc_id; }
            set { _secc_id = value; }
        }

        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        public int Localidad_Id
        {
            get { return _localidad_id; }
            set { _localidad_id = value; }
        }
        #endregion
    }
}
