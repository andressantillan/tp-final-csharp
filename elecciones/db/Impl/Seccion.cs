﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class Seccion : CommonObj, IAccessDB<Seccion>, ITable
    {
        //"id","nombre"
        private string[] _columns = { "id", "nombre" };

        public List<Seccion> findAll()
        {
            return this.findAll(null);
        }
        public List<Seccion> findAll(string criterio)
        {
            return ManagerDB<Seccion>.findAll(criterio);
        }
        public Seccion findbykey(params object[] key)
        {
            //"id","nombre"
            Seccion sec = (Seccion)ManagerDB<Seccion>.findbyKey(key);
            this.Id = sec.Id;
            this.Nombre = sec.Nombre;
            
            this.IsNew = false;
            return this;
        }

        public bool saveObj()
        {
            return ManagerDB<Seccion>.saveObject(this);
        }

        public string TableName
        {
            get { return "seccion"; }
        }

        public string KeyTable
        {
            get { return "id"; }
        }

        public void initialize(System.Data.DataRow dr)
        {
            //"id","nombre"
            this._id = Int32.Parse(dr[_columns[0]].ToString());
            this._nombre = dr[_columns[1]].ToString();
            
            this.IsNew = false;
        }

        public string[] columns
        {
            get { return _columns; }
        }

        private string[] list_values()
        {
            //"id","nombre"
            string[] values = { (this.IsNew?"":_columns[0] + "=")+this._id.ToString(),
								(this.IsNew?"":_columns[1] + "=")+this._nombre,
							  };
            return values;
        }

        public string SqlString
        {
            get
            {
                string vvalues = String.Join(",", this.list_values());
                string sqliu = (this.IsNew ? "insert into {0} ({1}) values ({2})" : "update  {0} set {1} where {2}");
                return String.Format(sqliu, this.TableName, (this.IsNew ? String.Join(",", _columns) : vvalues), (this.IsNew ? vvalues : String.Format("id = {0}", this.Id)));
            }
        }

        public void setKeyValue(object valueId)
        {
            throw new NotImplementedException();
        }

        public string sqlKeyWhere(params object[] values)
        {
            return String.Format("id = {0}", values[0]);
        }
    }
}
