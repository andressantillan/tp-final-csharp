﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class Circuito : CommonObj, IAccessDB<Circuito>, ITable
    {
        private string[] _columns = { "id","letra", "secc_id", "nombre", "localidad_id" };
        
        public List<Circuito> findAll()
        {
            return this.findAll(null);
        }
        public List<Circuito> findAll(string criterio)
        {
            return ManagerDB<Circuito>.findAll(criterio);
        }
        public Circuito findbykey(params object[] key)
        {
            // "id","letra", "secc_id", "nombre", "localidad_id"
            Circuito cir = (Circuito)ManagerDB<Circuito>.findbyKey(key);
            this.Id = cir.Id;
            this.Letra = cir.Letra;
            this.Secc_Id = cir.Secc_Id;
            this.Nombre = cir.Nombre;
            this.Localidad_Id = cir.Localidad_Id;
            this.IsNew = false;
            return this;
        }

        public bool saveObj()
        {
            return ManagerDB<Circuito>.saveObject(this);
        }

        public string TableName
        {
            get { return "circuito"; }
        }

        public string KeyTable
        {
            get { return "id"; }
        }

        public void initialize(System.Data.DataRow dr)
        {
            // "id","letra", "secc_id", "nombre", "localidad_id"
            this._id = Int32.Parse(dr[_columns[0]].ToString());
			this._letra = dr[_columns[1]].ToString();
            this._secc_id = Int32.Parse(dr[_columns[2]].ToString());
            this._nombre = dr[_columns[3]].ToString();
            this._localidad_id =Int32.Parse(dr[_columns[4]].ToString());
         
            this.IsNew = false;
        }

        public string[] columns
        {
            get { return _columns; }
        }

        private string[] list_values()
        {
            // "id","letra", "secc_id", "nombre", "localidad_id"
            string[] values = { (this.IsNew?"":_columns[0] + "=")+this._id.ToString(),
								(this.IsNew?"":_columns[1] + "=")+this._letra,
                                (this.IsNew?"":_columns[2] + "=")+this._secc_id.ToString(),
								(this.IsNew?"":_columns[3] + "=")+this._nombre,
								(this.IsNew?"":_columns[4] + "=")+this._localidad_id.ToString()
                              };
            return values;
        }

        public string SqlString
        {
            get
            {
                string vvalues = String.Join(",", this.list_values());
                string sqliu = (this.IsNew ? "insert into {0} ({1}) values ({2})" : "update  {0} set {1} where {2}");
                return String.Format(sqliu, this.TableName, (this.IsNew ? String.Join(",", _columns) : vvalues), (this.IsNew ? vvalues : String.Format("id = {0}", this.Id)));
            }
        }

        public void setKeyValue(object valueId)
        {
            throw new NotImplementedException();
        }

        public string sqlKeyWhere(params object[] values)
        {
            return String.Format("id = {0}", values[0]);
        }
    }
}
