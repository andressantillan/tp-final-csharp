﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace elecciones.db
{
    public partial class CategoriaEleccion : CommonObj, IAccessDB<CategoriaEleccion>, ITable
    {
        private string[] _columns = { "id","nom_categoria" };
        public List<CategoriaEleccion> findAll()
        {
            return this.findAll(null);
        }
        public List<CategoriaEleccion> findAll(string criterio)
        {
            return ManagerDB<CategoriaEleccion>.findAll(criterio);
        }
        public CategoriaEleccion findbykey(params object[] key)
        {
            CategoriaEleccion ce = (CategoriaEleccion)ManagerDB<CategoriaEleccion>.findbyKey(key);
            this.Id = ce.Id;
            this.Nom_Categoria = ce.Nom_Categoria;
            this.IsNew = false;
            return this;
        }
        public bool saveObj()
        {
            return ManagerDB<CategoriaEleccion>.saveObject(this);
        }

        public string TableName
        {
            get { return "categ_eleccion"; }
        }

        public string KeyTable
        {
            get { return "id"; }
        }

        public void initialize(System.Data.DataRow dr)
        {
			// "id","nom_categoria"
            this._id = Int32.Parse(dr[_columns[0]].ToString());
			this._nom_categoria = dr[_columns[1]].ToString();
            this.IsNew = false;
        }

        public string[] columns
        {
            get { return _columns; }
        }

        private string[] list_values()
        {
			// "id","nom_categoria"
            string[] values = { (this.IsNew?"":_columns[0] + "=")+this._id.ToString(),
								(this.IsNew?"":_columns[1] + "=")+this._nom_categoria
                              };
            return values;
        }

        public string SqlString
        {
            get
            {
                string vvalues = String.Join(",", this.list_values());
                string sqliu = (this.IsNew ? "insert into {0} ({1}) values ({2})" : "update  {0} set {1} where {2}");
                return String.Format(sqliu, this.TableName, (this.IsNew ? String.Join(",", _columns) : vvalues), (this.IsNew ? vvalues : String.Format("id = {0}", this.Id)));
            }
        }

        public void setKeyValue(object valueId)
        {
            throw new NotImplementedException();
        }

        public string sqlKeyWhere(params object[] values)
        {
            return String.Format("id = {0}", values[0]);
        }
    }
}
