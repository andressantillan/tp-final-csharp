﻿namespace elecciones
{
    partial class FrmListadoMesasTw
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.MesaCbo = new System.Windows.Forms.ComboBox();
            this.AceptarBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Seleccione:";
            // 
            // MesaCbo
            // 
            this.MesaCbo.DisplayMember = "Nro_Mesa";
            this.MesaCbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MesaCbo.FormattingEnabled = true;
            this.MesaCbo.Location = new System.Drawing.Point(118, 23);
            this.MesaCbo.Name = "MesaCbo";
            this.MesaCbo.Size = new System.Drawing.Size(121, 24);
            this.MesaCbo.TabIndex = 2;
            this.MesaCbo.ValueMember = "Id";
            // 
            // AceptarBtn
            // 
            this.AceptarBtn.Location = new System.Drawing.Point(119, 78);
            this.AceptarBtn.Name = "AceptarBtn";
            this.AceptarBtn.Size = new System.Drawing.Size(104, 28);
            this.AceptarBtn.TabIndex = 3;
            this.AceptarBtn.Text = "Aceptar";
            this.AceptarBtn.UseVisualStyleBackColor = true;
            this.AceptarBtn.Click += new System.EventHandler(this.AceptarBtn_Click);
            // 
            // FrmListadoMesasTw
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 152);
            this.Controls.Add(this.AceptarBtn);
            this.Controls.Add(this.MesaCbo);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmListadoMesasTw";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mesas de Trelew";
            this.Load += new System.EventHandler(this.FrmListadoMesasTw_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox MesaCbo;
        private System.Windows.Forms.Button AceptarBtn;
    }
}