﻿namespace elecciones
{
    partial class FrmCargaVotoMesa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SeccionTxt = new System.Windows.Forms.TextBox();
            this.CircuitoTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Int151Txt = new System.Windows.Forms.TextBox();
            this.Int152Txt = new System.Windows.Forms.TextBox();
            this.Int153Txt = new System.Windows.Forms.TextBox();
            this.Int1103Txt = new System.Windows.Forms.TextBox();
            this.Int1102Txt = new System.Windows.Forms.TextBox();
            this.Int1101Txt = new System.Windows.Forms.TextBox();
            this.Int1274Txt = new System.Windows.Forms.TextBox();
            this.Int1104Txt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.IntTotalTxt = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.ConcTotalTxt = new System.Windows.Forms.TextBox();
            this.Conc1274Txt = new System.Windows.Forms.TextBox();
            this.Conc1104Txt = new System.Windows.Forms.TextBox();
            this.Conc1103Txt = new System.Windows.Forms.TextBox();
            this.Conc1102Txt = new System.Windows.Forms.TextBox();
            this.Conc1101Txt = new System.Windows.Forms.TextBox();
            this.Conc153Txt = new System.Windows.Forms.TextBox();
            this.Conc152Txt = new System.Windows.Forms.TextBox();
            this.Conc151Txt = new System.Windows.Forms.TextBox();
            this.GuardarBtn = new System.Windows.Forms.Button();
            this.CancelarBtn = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.CantidadElectoresTxt = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.NroMesaTxt = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nro.Mesa:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Seccion:";
            // 
            // SeccionTxt
            // 
            this.SeccionTxt.Enabled = false;
            this.SeccionTxt.Location = new System.Drawing.Point(107, 59);
            this.SeccionTxt.Name = "SeccionTxt";
            this.SeccionTxt.Size = new System.Drawing.Size(221, 22);
            this.SeccionTxt.TabIndex = 3;
            // 
            // CircuitoTxt
            // 
            this.CircuitoTxt.Enabled = false;
            this.CircuitoTxt.Location = new System.Drawing.Point(428, 56);
            this.CircuitoTxt.Name = "CircuitoTxt";
            this.CircuitoTxt.Size = new System.Drawing.Size(221, 22);
            this.CircuitoTxt.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(360, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Circuito:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 17);
            this.label4.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(75, 244);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "MAPU/MST";
            // 
            // Int151Txt
            // 
            this.Int151Txt.Location = new System.Drawing.Point(257, 57);
            this.Int151Txt.MaxLength = 3;
            this.Int151Txt.Name = "Int151Txt";
            this.Int151Txt.Size = new System.Drawing.Size(100, 22);
            this.Int151Txt.TabIndex = 6;
            this.Int151Txt.Tag = "1";
            // 
            // Int152Txt
            // 
            this.Int152Txt.Location = new System.Drawing.Point(257, 89);
            this.Int152Txt.MaxLength = 3;
            this.Int152Txt.Name = "Int152Txt";
            this.Int152Txt.Size = new System.Drawing.Size(100, 22);
            this.Int152Txt.TabIndex = 8;
            this.Int152Txt.Tag = "3";
            // 
            // Int153Txt
            // 
            this.Int153Txt.Location = new System.Drawing.Point(257, 120);
            this.Int153Txt.MaxLength = 3;
            this.Int153Txt.Name = "Int153Txt";
            this.Int153Txt.Size = new System.Drawing.Size(100, 22);
            this.Int153Txt.TabIndex = 10;
            this.Int153Txt.Tag = "5";
            // 
            // Int1103Txt
            // 
            this.Int1103Txt.Location = new System.Drawing.Point(257, 212);
            this.Int1103Txt.MaxLength = 3;
            this.Int1103Txt.Name = "Int1103Txt";
            this.Int1103Txt.Size = new System.Drawing.Size(100, 22);
            this.Int1103Txt.TabIndex = 16;
            this.Int1103Txt.Tag = "11";
            // 
            // Int1102Txt
            // 
            this.Int1102Txt.Location = new System.Drawing.Point(257, 183);
            this.Int1102Txt.MaxLength = 3;
            this.Int1102Txt.Name = "Int1102Txt";
            this.Int1102Txt.Size = new System.Drawing.Size(100, 22);
            this.Int1102Txt.TabIndex = 14;
            this.Int1102Txt.Tag = "9";
            // 
            // Int1101Txt
            // 
            this.Int1101Txt.Location = new System.Drawing.Point(257, 153);
            this.Int1101Txt.MaxLength = 3;
            this.Int1101Txt.Name = "Int1101Txt";
            this.Int1101Txt.Size = new System.Drawing.Size(100, 22);
            this.Int1101Txt.TabIndex = 12;
            this.Int1101Txt.Tag = "7";
            // 
            // Int1274Txt
            // 
            this.Int1274Txt.Location = new System.Drawing.Point(257, 274);
            this.Int1274Txt.MaxLength = 3;
            this.Int1274Txt.Name = "Int1274Txt";
            this.Int1274Txt.Size = new System.Drawing.Size(100, 22);
            this.Int1274Txt.TabIndex = 20;
            this.Int1274Txt.Tag = "15";
            // 
            // Int1104Txt
            // 
            this.Int1104Txt.Location = new System.Drawing.Point(257, 244);
            this.Int1104Txt.MaxLength = 3;
            this.Int1104Txt.Name = "Int1104Txt";
            this.Int1104Txt.Size = new System.Drawing.Size(100, 22);
            this.Int1104Txt.TabIndex = 18;
            this.Int1104Txt.Tag = "13";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(75, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 17);
            this.label6.TabIndex = 20;
            this.label6.Text = "P.I.C.H.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 244);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 17);
            this.label7.TabIndex = 21;
            this.label7.Text = "1104";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 17);
            this.label8.TabIndex = 22;
            this.label8.Text = "151";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 17);
            this.label9.TabIndex = 23;
            this.label9.Text = "152";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(75, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(135, 17);
            this.label10.TabIndex = 24;
            this.label10.Text = "MOV.POLO SOCIAL";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(75, 120);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 17);
            this.label11.TabIndex = 26;
            this.label11.Text = "P.S.A.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 120);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 17);
            this.label12.TabIndex = 25;
            this.label12.Text = "153";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(75, 153);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(158, 17);
            this.label15.TabIndex = 30;
            this.label15.Text = "A.CHUBUT AL FRENTE";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 153);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 17);
            this.label16.TabIndex = 29;
            this.label16.Text = "1101";
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(75, 183);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(168, 22);
            this.label17.TabIndex = 32;
            this.label17.Text = "A.FRENTE PATRIOTICO";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 183);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 17);
            this.label18.TabIndex = 31;
            this.label18.Text = "1102";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(75, 212);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(168, 22);
            this.label19.TabIndex = 34;
            this.label19.Text = "A.CAMBIEMOS";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 212);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(40, 17);
            this.label20.TabIndex = 33;
            this.label20.Text = "1103";
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(75, 274);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(168, 22);
            this.label21.TabIndex = 36;
            this.label21.Text = "PARTIDO X TRELEW";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(15, 274);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 17);
            this.label22.TabIndex = 35;
            this.label22.Text = "1274";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(79, 319);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(168, 22);
            this.label27.TabIndex = 43;
            this.label27.Text = "TOTAL VOTOS";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // IntTotalTxt
            // 
            this.IntTotalTxt.Enabled = false;
            this.IntTotalTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IntTotalTxt.Location = new System.Drawing.Point(257, 319);
            this.IntTotalTxt.MaxLength = 3;
            this.IntTotalTxt.Name = "IntTotalTxt";
            this.IntTotalTxt.Size = new System.Drawing.Size(100, 26);
            this.IntTotalTxt.TabIndex = 32;
            this.IntTotalTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(261, 18);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(96, 17);
            this.label28.TabIndex = 44;
            this.label28.Text = "INTENDENTE";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.ConcTotalTxt);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Int151Txt);
            this.groupBox1.Controls.Add(this.Int152Txt);
            this.groupBox1.Controls.Add(this.Int153Txt);
            this.groupBox1.Controls.Add(this.Int1101Txt);
            this.groupBox1.Controls.Add(this.Conc1274Txt);
            this.groupBox1.Controls.Add(this.Int1102Txt);
            this.groupBox1.Controls.Add(this.Conc1104Txt);
            this.groupBox1.Controls.Add(this.Int1103Txt);
            this.groupBox1.Controls.Add(this.Conc1103Txt);
            this.groupBox1.Controls.Add(this.Int1104Txt);
            this.groupBox1.Controls.Add(this.Conc1102Txt);
            this.groupBox1.Controls.Add(this.Int1274Txt);
            this.groupBox1.Controls.Add(this.Conc1101Txt);
            this.groupBox1.Controls.Add(this.Conc153Txt);
            this.groupBox1.Controls.Add(this.Conc152Txt);
            this.groupBox1.Controls.Add(this.Conc151Txt);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.IntTotalTxt);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Location = new System.Drawing.Point(80, 87);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(528, 363);
            this.groupBox1.TabIndex = 45;
            this.groupBox1.TabStop = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(384, 18);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(98, 17);
            this.label29.TabIndex = 60;
            this.label29.Text = "CONCEJALES";
            // 
            // ConcTotalTxt
            // 
            this.ConcTotalTxt.Enabled = false;
            this.ConcTotalTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConcTotalTxt.Location = new System.Drawing.Point(382, 319);
            this.ConcTotalTxt.MaxLength = 3;
            this.ConcTotalTxt.Name = "ConcTotalTxt";
            this.ConcTotalTxt.Size = new System.Drawing.Size(100, 26);
            this.ConcTotalTxt.TabIndex = 33;
            this.ConcTotalTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Conc1274Txt
            // 
            this.Conc1274Txt.Location = new System.Drawing.Point(382, 274);
            this.Conc1274Txt.MaxLength = 3;
            this.Conc1274Txt.Name = "Conc1274Txt";
            this.Conc1274Txt.Size = new System.Drawing.Size(100, 22);
            this.Conc1274Txt.TabIndex = 21;
            this.Conc1274Txt.Tag = "16";
            // 
            // Conc1104Txt
            // 
            this.Conc1104Txt.Location = new System.Drawing.Point(382, 244);
            this.Conc1104Txt.MaxLength = 3;
            this.Conc1104Txt.Name = "Conc1104Txt";
            this.Conc1104Txt.Size = new System.Drawing.Size(100, 22);
            this.Conc1104Txt.TabIndex = 19;
            this.Conc1104Txt.Tag = "14";
            // 
            // Conc1103Txt
            // 
            this.Conc1103Txt.Location = new System.Drawing.Point(382, 212);
            this.Conc1103Txt.MaxLength = 3;
            this.Conc1103Txt.Name = "Conc1103Txt";
            this.Conc1103Txt.Size = new System.Drawing.Size(100, 22);
            this.Conc1103Txt.TabIndex = 17;
            this.Conc1103Txt.Tag = "12";
            // 
            // Conc1102Txt
            // 
            this.Conc1102Txt.Location = new System.Drawing.Point(382, 183);
            this.Conc1102Txt.MaxLength = 3;
            this.Conc1102Txt.Name = "Conc1102Txt";
            this.Conc1102Txt.Size = new System.Drawing.Size(100, 22);
            this.Conc1102Txt.TabIndex = 15;
            this.Conc1102Txt.Tag = "10";
            // 
            // Conc1101Txt
            // 
            this.Conc1101Txt.Location = new System.Drawing.Point(382, 153);
            this.Conc1101Txt.MaxLength = 3;
            this.Conc1101Txt.Name = "Conc1101Txt";
            this.Conc1101Txt.Size = new System.Drawing.Size(100, 22);
            this.Conc1101Txt.TabIndex = 13;
            this.Conc1101Txt.Tag = "8";
            // 
            // Conc153Txt
            // 
            this.Conc153Txt.Location = new System.Drawing.Point(382, 120);
            this.Conc153Txt.MaxLength = 3;
            this.Conc153Txt.Name = "Conc153Txt";
            this.Conc153Txt.Size = new System.Drawing.Size(100, 22);
            this.Conc153Txt.TabIndex = 11;
            this.Conc153Txt.Tag = "6";
            // 
            // Conc152Txt
            // 
            this.Conc152Txt.Location = new System.Drawing.Point(382, 89);
            this.Conc152Txt.MaxLength = 3;
            this.Conc152Txt.Name = "Conc152Txt";
            this.Conc152Txt.Size = new System.Drawing.Size(100, 22);
            this.Conc152Txt.TabIndex = 9;
            this.Conc152Txt.Tag = "4";
            // 
            // Conc151Txt
            // 
            this.Conc151Txt.Location = new System.Drawing.Point(382, 57);
            this.Conc151Txt.MaxLength = 3;
            this.Conc151Txt.Name = "Conc151Txt";
            this.Conc151Txt.Size = new System.Drawing.Size(100, 22);
            this.Conc151Txt.TabIndex = 7;
            this.Conc151Txt.Tag = "2";
            // 
            // GuardarBtn
            // 
            this.GuardarBtn.Location = new System.Drawing.Point(307, 466);
            this.GuardarBtn.Name = "GuardarBtn";
            this.GuardarBtn.Size = new System.Drawing.Size(98, 34);
            this.GuardarBtn.TabIndex = 46;
            this.GuardarBtn.Text = "Guardar";
            this.GuardarBtn.UseVisualStyleBackColor = true;
            this.GuardarBtn.Click += new System.EventHandler(this.GuardarBtn_Click);
            // 
            // CancelarBtn
            // 
            this.CancelarBtn.Location = new System.Drawing.Point(497, 466);
            this.CancelarBtn.Name = "CancelarBtn";
            this.CancelarBtn.Size = new System.Drawing.Size(98, 34);
            this.CancelarBtn.TabIndex = 47;
            this.CancelarBtn.Text = "Cancelar";
            this.CancelarBtn.UseVisualStyleBackColor = true;
            this.CancelarBtn.Click += new System.EventHandler(this.CancelarBtn_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(278, 11);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(118, 29);
            this.label30.TabIndex = 48;
            this.label30.Text = "TRELEW";
            // 
            // CantidadElectoresTxt
            // 
            this.CantidadElectoresTxt.Enabled = false;
            this.CantidadElectoresTxt.Location = new System.Drawing.Point(555, 8);
            this.CantidadElectoresTxt.Name = "CantidadElectoresTxt";
            this.CantidadElectoresTxt.Size = new System.Drawing.Size(103, 22);
            this.CantidadElectoresTxt.TabIndex = 1;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(444, 11);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(105, 17);
            this.label31.TabIndex = 50;
            this.label31.Text = "Electores Mesa";
            // 
            // NroMesaTxt
            // 
            this.NroMesaTxt.Enabled = false;
            this.NroMesaTxt.Location = new System.Drawing.Point(115, 23);
            this.NroMesaTxt.Name = "NroMesaTxt";
            this.NroMesaTxt.Size = new System.Drawing.Size(120, 22);
            this.NroMesaTxt.TabIndex = 0;
            // 
            // FrmCargaVotoMesa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(668, 529);
            this.Controls.Add(this.NroMesaTxt);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.CantidadElectoresTxt);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.CancelarBtn);
            this.Controls.Add(this.GuardarBtn);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CircuitoTxt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.SeccionTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCargaVotoMesa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Carga de datos de Mesa ";
            this.Load += new System.EventHandler(this.FrmCargaVotoMesa_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SeccionTxt;
        private System.Windows.Forms.TextBox CircuitoTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Int151Txt;
        private System.Windows.Forms.TextBox Int152Txt;
        private System.Windows.Forms.TextBox Int153Txt;
        private System.Windows.Forms.TextBox Int1103Txt;
        private System.Windows.Forms.TextBox Int1102Txt;
        private System.Windows.Forms.TextBox Int1101Txt;
        private System.Windows.Forms.TextBox Int1274Txt;
        private System.Windows.Forms.TextBox Int1104Txt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox IntTotalTxt;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox ConcTotalTxt;
        private System.Windows.Forms.TextBox Conc1274Txt;
        private System.Windows.Forms.TextBox Conc1104Txt;
        private System.Windows.Forms.TextBox Conc1103Txt;
        private System.Windows.Forms.TextBox Conc1102Txt;
        private System.Windows.Forms.TextBox Conc1101Txt;
        private System.Windows.Forms.TextBox Conc153Txt;
        private System.Windows.Forms.TextBox Conc152Txt;
        private System.Windows.Forms.TextBox Conc151Txt;
        private System.Windows.Forms.Button GuardarBtn;
        private System.Windows.Forms.Button CancelarBtn;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox CantidadElectoresTxt;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox NroMesaTxt;
    }
}