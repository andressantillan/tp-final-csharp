﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using elecciones.db;
namespace elecciones
{
     public delegate void MesaSeleccionada(Mesa MesaSelected);
     public partial class FrmListadoMesasTw : Form
    {
        public event MesaSeleccionada Seleccion;
        public FrmListadoMesasTw()
        {
            InitializeComponent();
        }

        private void FrmListadoMesasTw_Load(object sender, EventArgs e)
        {
            //Listar las Mesas de Trelew
            this.MesaCbo.DataSource = ManagerDB<Mesa>.findAll("nro_mesa>= 88 and  nro_mesa<=336");
        }

        private void AceptarBtn_Click(object sender, EventArgs e)
        {
            Mesa Mesa;
            if (this.Seleccion != null)
            { 
                Mesa = this.MesaCbo.Items[this.MesaCbo.SelectedIndex] as Mesa;
                this.Seleccion(Mesa);
                this.Dispose();
            }
        }
    }
}
