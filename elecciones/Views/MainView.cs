﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Npgsql; // Libreria para conectar con PostgreSQL
using elecciones.db; // incluir libreria para poder acceder a los objetos de negocios.

namespace elecciones
{
    public enum OperacionForm { frmAlta = 1, frmModificacion=2 }
    public partial class MainView : Form
    {
        FrmCargaVotoMesa FrmCarga = null;
        FrmListadoMesasTw FrmListadoMesas = null;
        List<TelegramaActa> ListaActaCargados = new List<TelegramaActa>();

        public MainView()
        {
            InitializeComponent();
        }

        private void MainView_Load(object sender, EventArgs e)
        {
            
        }

        void FrmListadoMesas_Seleccion(Mesa MesaSelected)
        {
            if (FrmCarga == null)
            {
                FrmCarga = new FrmCargaVotoMesa();
                FrmCarga.InfoTelegramaCargado += new TelegramaCargado(FrmCarga_InfoTelegramaCargado);
                FrmListadoMesas.Dispose();
                // verificar desde la base si ya existe telegrama cargado o no, segun eso cambia el metodo a llamar
                // shoAlta o showModificacion
                TelegramaActa tel = ManagerDB<TelegramaActa>.findbyKey(MesaSelected.Nro_Mesa);
                if(tel.Cargado)
                    FrmCarga.ShowModificacion(tel);
                else
                    FrmCarga.ShowAlta(MesaSelected);
            }
        }
        // Evento que indica que se completo la carga de datos de votos - GuardarBtn-Click
        void FrmCarga_InfoTelegramaCargado(TelegramaActa Telegrama)
        {
            
            // Carga Listado de Mesa
        }

        private void EscuelasButton_Click(object sender, EventArgs e)
        {
            this.dataGrid.DataSource = ManagerDB<Escuela>.findAll();
        }

        private void CargaMesaBtn_Click(object sender, EventArgs e)
        {
            if (FrmListadoMesas == null)
            {
                FrmListadoMesas = new FrmListadoMesasTw();
                FrmListadoMesas.Seleccion += new MesaSeleccionada(FrmListadoMesas_Seleccion);               
            }
            FrmListadoMesas.ShowDialog();
        }

        private void ResultadosBtn_Click(object sender, EventArgs e)
        {
            // verificar como recuperar los datos 
            //DataTable dt = AccessDB.getExecuteSQL("... sql...");
            // poner resultados en un DataGridView-....
        }
    }
}
