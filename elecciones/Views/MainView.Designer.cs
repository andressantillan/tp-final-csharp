﻿namespace elecciones
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EscuelasButton = new System.Windows.Forms.Button();
            this.dataGrid = new System.Windows.Forms.DataGridView();
            this.CargaMesaBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ResultadosBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.GridMesas = new System.Windows.Forms.DataGridView();
            this.Mesa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridMesas)).BeginInit();
            this.SuspendLayout();
            // 
            // EscuelasButton
            // 
            this.EscuelasButton.Location = new System.Drawing.Point(43, 47);
            this.EscuelasButton.Margin = new System.Windows.Forms.Padding(5);
            this.EscuelasButton.Name = "EscuelasButton";
            this.EscuelasButton.Size = new System.Drawing.Size(116, 34);
            this.EscuelasButton.TabIndex = 0;
            this.EscuelasButton.Text = "Escuelas";
            this.EscuelasButton.UseVisualStyleBackColor = true;
            this.EscuelasButton.Click += new System.EventHandler(this.EscuelasButton_Click);
            // 
            // dataGrid
            // 
            this.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid.Location = new System.Drawing.Point(183, 18);
            this.dataGrid.Margin = new System.Windows.Forms.Padding(5);
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.Size = new System.Drawing.Size(713, 222);
            this.dataGrid.TabIndex = 4;
            // 
            // CargaMesaBtn
            // 
            this.CargaMesaBtn.Location = new System.Drawing.Point(23, 359);
            this.CargaMesaBtn.Margin = new System.Windows.Forms.Padding(4);
            this.CargaMesaBtn.Name = "CargaMesaBtn";
            this.CargaMesaBtn.Size = new System.Drawing.Size(133, 58);
            this.CargaMesaBtn.TabIndex = 6;
            this.CargaMesaBtn.Text = "Carga de Mesa";
            this.CargaMesaBtn.UseVisualStyleBackColor = true;
            this.CargaMesaBtn.Click += new System.EventHandler(this.CargaMesaBtn_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 314);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 41);
            this.label1.TabIndex = 7;
            this.label1.Text = "Registro de Mesas\r\nEscrutinio\r\n";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ResultadosBtn
            // 
            this.ResultadosBtn.Location = new System.Drawing.Point(28, 453);
            this.ResultadosBtn.Name = "ResultadosBtn";
            this.ResultadosBtn.Size = new System.Drawing.Size(128, 48);
            this.ResultadosBtn.TabIndex = 8;
            this.ResultadosBtn.Text = "Resultados";
            this.ResultadosBtn.UseVisualStyleBackColor = true;
            this.ResultadosBtn.Click += new System.EventHandler(this.ResultadosBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "LISTADOS";
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(31, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 40);
            this.label3.TabIndex = 10;
            this.label3.Text = "Pendientes en formularios separados";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Items.AddRange(new object[] {
            "Circuitos",
            "Secciones",
            "Partidos",
            "ListaPartidos",
            "Localidades"});
            this.listBox1.Location = new System.Drawing.Point(43, 140);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(113, 100);
            this.listBox1.TabIndex = 11;
            // 
            // GridMesas
            // 
            this.GridMesas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridMesas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Mesa,
            this.Fecha});
            this.GridMesas.Location = new System.Drawing.Point(183, 300);
            this.GridMesas.Name = "GridMesas";
            this.GridMesas.RowTemplate.Height = 24;
            this.GridMesas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridMesas.Size = new System.Drawing.Size(713, 201);
            this.GridMesas.TabIndex = 12;
            // 
            // Mesa
            // 
            this.Mesa.DataPropertyName = "Nro_Mesa";
            this.Mesa.HeaderText = "Mesa";
            this.Mesa.Name = "Mesa";
            this.Mesa.Width = 150;
            // 
            // Fecha
            // 
            this.Fecha.HeaderText = "Fecha Carga";
            this.Fecha.Name = "Fecha";
            this.Fecha.Width = 180;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(372, 266);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(243, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "Implementar en formulario separados";
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 533);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.GridMesas);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ResultadosBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CargaMesaBtn);
            this.Controls.Add(this.dataGrid);
            this.Controls.Add(this.EscuelasButton);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ElectorApp: Registro de informacion de Elecciones - Generales 2019";
            this.Load += new System.EventHandler(this.MainView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridMesas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button EscuelasButton;
        private System.Windows.Forms.DataGridView dataGrid;

        private System.Windows.Forms.Button CargaMesaBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ResultadosBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.DataGridView GridMesas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mesa;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha;
        private System.Windows.Forms.Label label4;
    }
}