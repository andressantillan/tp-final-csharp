﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using elecciones.db;

namespace elecciones
{
    public delegate void TelegramaCargado(TelegramaActa Telegrama);
    public partial class FrmCargaVotoMesa : Form
    {
        public event TelegramaCargado InfoTelegramaCargado;
        private Mesa MesaCargaVotos;
        private TelegramaActa Telegrama;
        private OperacionForm _operacion;
        public OperacionForm Operacion
        {
            get
            {
                return _operacion;
            }
            set
            {
                _operacion = value;
                if (_operacion == OperacionForm.frmAlta)
                {
                    this.Text = "Formulario de Carga de datos de Votos de Mesa - Trelew";
                }
                if (_operacion == OperacionForm.frmModificacion)
                {
                    this.Text = "Formulario de Actualización de datos de Votos de Mesa - Trelew";
                }
            }
        }
        public FrmCargaVotoMesa()
        {
            InitializeComponent();
        }

        public void ShowAlta(Mesa MesaObj)
        {
            this._operacion = OperacionForm.frmAlta;
            this.NroMesaTxt.Text = MesaObj.Nro_Mesa.ToString();
            this.CantidadElectoresTxt.Text = MesaObj.Cant_Electores.ToString();
            this.SeccionTxt.Text = MesaObj.Circuito.Seccion.Nombre;
            this.CircuitoTxt.Text = MesaObj.Circuito.Nombre;
            MesaCargaVotos = MesaObj;
            this.ShowDialog();
        }

        public void ShowModificacion(TelegramaActa Tel)
        {
            this._operacion = OperacionForm.frmModificacion;
            Telegrama = Tel;
            this.NroMesaTxt.Text = Tel.Mesa.Nro_Mesa.ToString();
            this.CantidadElectoresTxt.Text = Tel.Mesa.Cant_Electores.ToString();
            this.SeccionTxt.Text = Tel.Mesa.Circuito.Seccion.Nombre;
            this.CircuitoTxt.Text = Tel.Mesa.Circuito.Nombre;
            Telegrama = Tel;
            // en modificacion de datos, se debe recuperar la informacion de cada lista/partido y categoria(4,5) en localidad (2-Trelew) 
            // se deben mostrar los votos ya registrados en los cuadros de texto.
            //ejemplo  -- campo Tag del cuadro de texto posee id de lista_partido_localidad.
            //recuperar votos de listaPartido 151 categoria Intendente
            List<TelegramaActaVotos> telVotos151Int_Col = ManagerDB<TelegramaActaVotos>.findAll(String.Format("lista_partido_id={0} and ",this.Int151Txt.Tag));
            this.Int151Txt.Text = telVotos151Int_Col[0].Votos.ToString();
            this.Int151Txt.Tag = telVotos151Int_Col[0].Id.ToString(); // guardo el Id de esta tupla en la base, al momento de actualizar los datos de votos se a que Id lo debo registrar

        }

        private void FrmCargaVotoMesa_Load(object sender, EventArgs e)
        {
            this.Int151Txt.KeyPress += new KeyPressEventHandler(Txt_KeyPress);
            this.Conc151Txt.KeyPress += new KeyPressEventHandler(Txt_KeyPress);
            //-- falta vincular funcion de validacion a los demas controles de ingreso de datos....
            
        }

        void Txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }        
        
        private void GuardarTelegramaHeader()
        {
            Telegrama = ManagerDB<TelegramaActa>.findbyKey(MesaCargaVotos.Id);
            Telegrama.Cargado = true;
            Telegrama.Fecha = DateTime.Now;
            Telegrama.saveObj();
        }
                
        private bool GuardarTelegramaDetail()
        {
            if (this.Operacion == OperacionForm.frmAlta)
            {
                //Guardar Datos de Lista 151
                TelegramaActaVotos TelVotInt151 = new TelegramaActaVotos();
                // Propiedad Tag tiene la informacion de la tabla lista_partido_localidad, para registrar la informacion de la
                // relacion de la categoria(4,5) en la localidad (2-Trelew) de los partidos enumerados.
                TelVotInt151.Lista_Partido_Loc_Id = Convert.ToInt32(this.Int151Txt.Tag);
                TelVotInt151.Teleg_Id = Telegrama.Id;
                TelVotInt151.Votos = Convert.ToInt32(this.Int151Txt.Text);
                TelVotInt151.saveObj();

                //Guardar Datos de Lista 152
                //Guardar Datos de Lista 153
                //Guardar Datos de Lista 163
                //Guardar Datos de Lista 1101
                //Guardar Datos de Lista 1102
                //Guardar Datos de Lista 1104
                //Guardar Datos de Lista 1274    
            }
            else // es modificacion
            {
                // en Propiedad Tag tengo el id para recuperar de la base.
                TelegramaActaVotos TelVotInt151 = ManagerDB<TelegramaActaVotos>.findbyKey(Convert.ToInt32(this.Int151Txt.Tag));
                TelVotInt151.Votos = Convert.ToInt32(this.Int151Txt.Text);
                TelVotInt151.saveObj(); // actualiza en la base.
            }
            return true;
        }

        private void GuardarBtn_Click(object sender, EventArgs e)
        {
            if (this._operacion == OperacionForm.frmAlta)
            {
                GuardarTelegramaHeader();
                if (GuardarTelegramaDetail())
                {
                    MessageBox.Show("Votos del telegrama registrados!", "Votos registrados...", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            else // es modificacion
            {
 
            }
            if (this.InfoTelegramaCargado != null)
            {
                InfoTelegramaCargado(Telegrama);
            }
        }

        private void CancelarBtn_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }        
    }
}
